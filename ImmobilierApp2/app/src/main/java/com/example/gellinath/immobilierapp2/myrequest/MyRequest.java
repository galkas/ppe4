package com.example.gellinath.immobilierapp2.myrequest;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.gellinath.immobilierapp2.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.util.HashMap;
import java.util.Map;

import static com.example.gellinath.immobilierapp2.MainActivity.ip;

public class MyRequest {

    private Context context;
    private RequestQueue queue;

    public MyRequest(Context context, RequestQueue queue) {
        this.context = context;
        this.queue = queue;
    }

    public void registerCR (final Integer no_compte_rendu, final String date_visite, final String proprietaire, final Integer annonce, final String visiteur, final String remarque, final String fonction_visiteur, final String financement_prevu, final Integer note_visiteur, final String synthese, final Integer contact, final newCRCallBack callback){

        String url = "http://"+ip+"/ppe4/add.php";

        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Map<String, String> errors = new HashMap<>();

                try {
                    JSONObject json = new JSONObject(response);

                    Boolean error = json.getBoolean("error");

                    if(!error){
                        callback.onSuccess("compte rendu ajouté");
                    }else{

                        JSONObject messages = json.getJSONObject("message");
                        if(messages.has("pris")){
                            errors.put("no_compte_rendu", messages.getString("pris"));
                        }

                        callback.inputErrors(errors);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("APP", response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                if(error  instanceof NetworkError){
                    callback.onError("impossible de se connecter");

                }else if(error instanceof VolleyError){
                    callback.onError("une erreur c'est produite");
                }

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> map = new HashMap<>();
                map.put("no_compte_rendu", no_compte_rendu.toString());
                map.put("date_visite", date_visite.toString());
                map.put("proprietaire", proprietaire);
                map.put("annonce", annonce.toString());
                map.put("visiteur", visiteur);
                map.put("remarque", remarque);
                map.put("fonction_visiteur", fonction_visiteur);
                map.put("financement_prevu", financement_prevu);
                map.put("note_visiteur", note_visiteur.toString());
                map.put("synthese", synthese);
                map.put("contact", contact.toString());


                return map;
            }
        };

        queue.add(request);

    }

    public interface newCRCallBack{
        void onSuccess(String message);
        void inputErrors(Map<String, String> errors);
        void onError(String message);
    }
}
