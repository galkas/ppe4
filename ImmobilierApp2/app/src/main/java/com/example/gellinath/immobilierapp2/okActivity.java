package com.example.gellinath.immobilierapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class okActivity extends AppCompatActivity {

    private Button btn_retour, btn_retour_new_CR;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ok);

        btn_retour_new_CR = (Button) findViewById(R.id.retour_new_cr);
        btn_retour = (Button) findViewById(R.id.btn_retour);

        btn_retour_new_CR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewCRActivity.class);
                startActivity(intent);
            }
        });

        btn_retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
