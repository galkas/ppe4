package com.example.gellinath.immobilierapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btn_newCR;
    public EditText myip;
    public static String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn_newCR = (Button) findViewById(R.id.compteRendu);
        myip = (EditText) findViewById(R.id.ip_serveur);


        btn_newCR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ip = String.valueOf(myip.getText().toString());
                Intent intent = new Intent(getApplicationContext(), NewCRActivity.class);
                startActivity(intent);
            }
        });
    }
}
