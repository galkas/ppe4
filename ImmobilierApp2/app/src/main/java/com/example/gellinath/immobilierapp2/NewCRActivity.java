package com.example.gellinath.immobilierapp2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.example.gellinath.immobilierapp2.myrequest.MyRequest;

import java.sql.Date;
import java.util.Map;

public class NewCRActivity extends AppCompatActivity {

    //création de elements de l'interface
    private Button btn_enregistrer;
    private EditText txtNoCompteRendu, myip, txtDateVisite, txtProprietaire, txtAnnonce, txtVisiteur, txtRemarque, txtFonctionVisiteur, txtFinancementPrevu, txtNoteVisiteur, txtSynthese, txtContact;
    private RequestQueue queue;
    private MyRequest request;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_cr);

        init();
    }

    /**
     * fonction executrice
     */
    public void init(){

        btn_enregistrer = (Button) findViewById(R.id.enregistrer);
        txtNoCompteRendu = (EditText) findViewById(R.id.no_compte_rendu);
        txtDateVisite = (EditText) findViewById(R.id.date_visite);
        txtProprietaire = (EditText) findViewById(R.id.Myproprietaire);
        txtAnnonce = (EditText) findViewById(R.id.annonce);
        txtVisiteur = (EditText) findViewById(R.id.visiteur);
        txtRemarque = (EditText) findViewById(R.id.remarque);
        txtFonctionVisiteur = (EditText) findViewById(R.id.fonction_visiteur);
        txtFinancementPrevu = (EditText) findViewById(R.id.financement_prevu);
        txtNoteVisiteur = (EditText) findViewById(R.id.note_visiteur);
        txtSynthese = (EditText) findViewById(R.id.synthese);
        txtContact = (EditText) findViewById(R.id.contact);

        queue = VolleySingleton.getInstance(this).getRequestQueue();
        request = new MyRequest(this, queue);

        btn_enregistrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                btn_enregistrer = (Button) findViewById(R.id.enregistrer);
                txtNoCompteRendu = (EditText) findViewById(R.id.no_compte_rendu);
                txtDateVisite = (EditText) findViewById(R.id.date_visite);
                txtProprietaire = (EditText) findViewById(R.id.Myproprietaire);
                txtAnnonce = (EditText) findViewById(R.id.annonce);
                txtVisiteur = (EditText) findViewById(R.id.visiteur);
                txtRemarque = (EditText) findViewById(R.id.remarque);
                txtFonctionVisiteur = (EditText) findViewById(R.id.fonction_visiteur);
                txtFinancementPrevu = (EditText) findViewById(R.id.financement_prevu);
                txtNoteVisiteur = (EditText) findViewById(R.id.note_visiteur);
                txtSynthese = (EditText) findViewById(R.id.synthese);
                txtContact = (EditText) findViewById(R.id.contact);

                try {
                    Integer noCompteRendu = Integer.parseInt(txtNoCompteRendu.getText().toString());
                    String dateVisite = String.valueOf(txtDateVisite.getText().toString());
                    String proprietaire = String.valueOf(txtProprietaire.getText());
                    Integer annonce = Integer.parseInt(txtAnnonce.getText().toString());
                    String visiteur = String.valueOf(txtVisiteur.getText());
                    String remarque = String.valueOf(txtRemarque.getText());
                    String fonctionVisiteur = String.valueOf(txtFonctionVisiteur.getText());
                    String financementPrevu = String.valueOf(txtFinancementPrevu.getText());
                    Integer noteVisiteur = Integer.parseInt(txtNoteVisiteur.getText().toString());
                    String synthese = String.valueOf(txtSynthese.getText());
                    Integer contact = Integer.parseInt(txtContact.getText().toString());


                    if (noCompteRendu != null && dateVisite != null && proprietaire != null && annonce != null && visiteur != null && remarque != null && fonctionVisiteur != null && financementPrevu != null && noteVisiteur != null && synthese != null && contact != null) {
                        request.registerCR(noCompteRendu, dateVisite, proprietaire, annonce, visiteur, remarque, fonctionVisiteur, financementPrevu, noteVisiteur, synthese, contact, new MyRequest.newCRCallBack() {
                            @Override
                            public void onSuccess(String message) {

                                Intent intent = new Intent(getApplicationContext(), okActivity.class);
                                intent.putExtra("insertion", message);
                                Toast.makeText(getApplicationContext(), "Création réussie", Toast.LENGTH_SHORT).show();
                                startActivity(intent);
                                finish();
                            }

                            @Override
                            public void inputErrors(Map<String, String> errors) {

                                if (errors.get("pris") != null) {
                                    txtNoCompteRendu.setError(errors.get("pris"));
                                } else {
                                    //txtNoCompteRendu.setErrorEnabled(false);
                                }
                            }

                            @Override
                            public void onError(String message) {

                            }
                        });
                    } else {
                        Toast.makeText(getApplicationContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
                    }
                }catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

}
