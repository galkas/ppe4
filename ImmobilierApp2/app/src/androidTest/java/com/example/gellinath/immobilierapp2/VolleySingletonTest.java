package com.example.gellinath.immobilierapp2;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;

import org.junit.Test;

import static org.junit.Assert.*;

public class VolleySingletonTest {

    private ImageLoader imageLoader;

    @Test
    public void getImageLoader() {

        assertEquals(imageLoader, VolleySingleton.getImageLoader());
    }
}